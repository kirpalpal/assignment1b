﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderMenu.aspx.cs" Inherits="assignment1b.OrderMenu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" ID="header" style="color:blue">User Details</asp:Label>
            <br />
            <br />
            <asp:Label runat="server" ID="userName">Name: </asp:Label><asp:TextBox runat="server" ID="txtUsername" placeholder="e.g. John Doe"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter name." ControlToValidate="txtUsername" ID="validname"></asp:RequiredFieldValidator>

            <br />
            <br />
            <asp:Label runat="server" ID="usernumber">Contact Number:</asp:Label><asp:TextBox runat="server" ID="txtUsernumber" placeholder="e.g. 1234567890"></asp:TextBox>
             <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter contact number." ControlToValidate="txtUsernumber" ID="validnumber"></asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:Label runat="server" ID="Useraddress">User Address:</asp:Label><asp:TextBox runat="server" ID="txtuseraddress" placeholder="e.g.123 ABC Street Brampton "></asp:TextBox> 
           <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter address." ControlToValidate="txtuseraddress" ID="validaddress"></asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:Label runat="server" ID="Useremail">User Email:</asp:Label><asp:TextBox runat="server" ID="txtuseremail" placeholder="e.g.abc23@gmail.com"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter email." ControlToValidate="txtuseremail" ID="validemail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtuseremail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            <br />
            <asp:Label runat="server" ID="Usergender">Gender: </asp:Label>
            <asp:DropDownList runat="server" ID="dropdownusergender">
                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                <asp:ListItem Text="Female" Value="F"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <asp:Label runat="server" ID="ordertype">Order Type</asp:Label>
            <asp:RadioButton runat="server" Text="Vegetarian" GroupName="ordertype" />
            <asp:RadioButton runat="server" Text="Non-Vegetarian" GroupName="ordertype" />
            <br />
            <br />
            <asp:Label runat="server" ID="payment">payment way</asp:Label>
            <br/>
            <br/>
            <asp:RadioButton runat="server" Text="cash" GroupName="payment" />
           <asp:RadioButton runat="server" Text="credit card" GroupName="payment" />  
             <br/>
               <br/>
            <asp:Label runat="server" ID="extras">Extras: </asp:Label>
            <br />
            <br />
            <asp:CheckBox ID="cbDip" runat="server" Text="Dippin Sauce" />
            <asp:CheckBox ID="cbColddrink" runat="server" Text="Colddrink" />
            <br/>
            <br/>
            <asp:Label runat="server" ID="quantity">Quantity:</asp:Label>
            <br/>
            <br/>
            <asp:DropDownList runat="server" ID="OrderSize">
                <asp:ListItem Text="Small size" Value="S"></asp:ListItem>
                <asp:ListItem Text="Medium size" Value="M"></asp:ListItem>
                <asp:ListItem Text="large size" Value="L"></asp:ListItem>
            </asp:DropDownList>            
            <br />
            <br />
            <asp:Button runat="server" ID="button" OnClick="Submitvalue" Text="Submit"/>
            <br />
            <br />
            <div runat="server" id="result">

            </div>
            <asp:ValidationSummary ID="ValidationSummary" runat="server" HeaderText="Validation summary" />
            
            <div runat="server" id="CustomertRes">

            </div>
            <div runat="server" id="itemsRes">

            </div>
            <div runat="server" id="billRes">

            </div>

            <footer runat="server" id="footer">


            </footer>
        </div>
    </form>


</body>
</html>
