﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment1b
{
    public class bill
    {
        public Customer c2;
        public items c3;

        public bill(Customer cls2, items cls3)
        {
            c2 = cls2;
            c3 = cls3;
        }

        public String PrintBill()
        {
            string paybill = "bill information<br>";
            paybill += "your total is :" + calculateOrder().ToString() + "<br/>";
            paybill += "name:" + c2.Customername + "<br/>";
            paybill += "email:" + c2.Customeremail + "<br/>";
            paybill += "phone number:" + c2.Customernumber + "<br/>";
            paybill += "address :" + c2.Customeraddress + "<br/>";
            // paybill += "gender:" + c3.gender + "<br/>";
           // paybill += "extra:" + c3.extras + "<br/>";
            paybill += "quantity:" + c3.quantity + "<br/>";

            return paybill;

        }

        public double calculateOrder()
        {
            double total = 0;
            if (c3.quantity == "S")
            {
                total = 4;
            }
            else if (c3.quantity == "M")
            {
                total = 5;
            }
            else if (c3.quantity == "L")
            {
                total = 13;
            }

            total += (c3.extras.Count() * 0.75);

            return total;
        }

    }
}