﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment1b
{
    public class Customer
    {

        public String customerName;
        public String customernumber;
        public String customeraddress;
        public String customeremail;

        public string Customername
        {
            get { return customerName; }
            set { customerName = value; }

        }

        public string Customernumber
        {
            get { return customernumber; }
            set { customernumber = value; }
        }

        public string Customeraddress
        {
            get { return customeraddress; }
            set { customeraddress = value; }
        }
        public string Customeremail
        {
            get { return customeremail; }
            set { customeremail = value; }
        }


    }
}